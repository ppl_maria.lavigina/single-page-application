ReactDOM.render(
  <ul>
    <li>
      <a href="home">Home</a>
    </li>
    <li>
      <a href="aboutUs">About us</a>
    </li>
    <li>
      <a href="gallery">Gallery</a>
    </li>
  </ul>,
  document.getElementById("myNav")
);

ReactDOM.render(<h1>Welcome to Edinburgh</h1>, document.getElementById("root"));

ReactDOM.render(
  <div>
    <img src="image1.jpg" width="250px"></img>
    <img src="image2.jpg" width="250px"></img>
    <img src="image3.jpg" width="250px" alt="image of Edinburgh"></img>
  </div>,
  document.getElementById("photosEdinburgh")
);
